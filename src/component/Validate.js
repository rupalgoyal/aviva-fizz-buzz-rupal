import React from 'react';
import FizzBuzz from './FizzBuzz';
class Validate extends React.Component{
	ValidateInput(value,min,max){      
		let limit = value;
		let validateResult = ''; 
		if(limit> min && limit <max){
			validateResult = 'valid';
		}
		else if(limit < min || limit >= max){
			validateResult ='Invalid Input, Please enter a number between 0-1000';
		}
		else{
			validateResult = 'Please Enter a value';
		}  
		return validateResult;
	}
	render(){
		let result = this.ValidateInput(this.props.value,this.props.min,this.props.max);
		if(result === 'valid'){
			return (
				<FizzBuzz value={this.props.value} />
			);
		}
		else{
			return (
				<div>
					{result}
				</div>        
			);  
		}
	}
}
export default Validate;