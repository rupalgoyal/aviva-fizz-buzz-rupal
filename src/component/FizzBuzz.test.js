import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import FizzBuzz from './FizzBuzz';
import Adapter from 'enzyme-adapter-react-15';
Enzyme.configure({ adapter: new Adapter() });

let wrapper;

it('FizzBuzz Test for number of Iterations', () => {
    const component = <FizzBuzz value={10} />;
    wrapper = shallow(component);
    expect(wrapper.find('li').length).toEqual(10);
});

it('Check for number of occurrences of Fizz', () => {
    const component = <FizzBuzz value={10} />;
    wrapper = shallow(component);
    expect(wrapper.find('.fizz').length).toEqual(3);
});

it('FizzBuzz Test for number of Iterations', () => {
    const component = <FizzBuzz value={10} />;
    wrapper = shallow(component);
    expect(wrapper.find('.buzz').length).toEqual(2);
});