import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import App from './App';
import InputComponent from './InputComponent';
import Adapter from 'enzyme-adapter-react-15';
Enzyme.configure({ adapter: new Adapter() });

let wrapper;
it('App should contain InputComponent', () => {
    wrapper = shallow(<App />);
    expect(wrapper.find('InputComponent').length).toEqual(1);
});