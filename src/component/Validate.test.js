import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import Validate from './Validate';
import Adapter from 'enzyme-adapter-react-15';
Enzyme.configure({ adapter: new Adapter() });
let wrapper;

it('should have min, max and value defined', () => {
    const component = <Validate value={10} min={0} max={100} />;
    wrapper = shallow(component);
	expect(wrapper.props().min).defined;
	expect(wrapper.props().max).defined;
	expect(wrapper.props().value).defined;
});

it('should have FizzBuzz componet as condition is valid', () => {
    const component = <Validate value={10} min={0} max={100} />;
    wrapper = shallow(component);
	expect(wrapper.find('FizzBuzz').length).toEqual(1);
	expect(wrapper.find('d1v').length).toEqual(0);
});

it('should have not have FizzBuzz component as condition is invalid', () => {
    const component = <Validate value={101} min={0} max={100} />;
    wrapper = shallow(component);
	expect(wrapper.find('FizzBuzz').length).toEqual(0);
	expect(wrapper.find('div').length).toEqual(1);
});