import React from 'react';
import Validate from './Validate';
class InputComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
         value: ' '
      }
      this.updateState = this.updateState.bind(this);
	};
	updateState(e) {
      this.setState({value: e.target.value});
	}
    render() {
        return (
        <div>
            <input
                type = "number"
                value = {this.state.value}
                onChange = {this.updateState} />
			<Validate value = {this.state.value} min = {0} max={1000}/>          
        </div>
        );        
    }
}
export default InputComponent;