import React from 'react';

class FizzBuzz extends React.Component{
  FizzBuzz(value){
      let limit = value;
      let fizzBuzzResult = [];
	  let today = new Date();
	  let day = today.getDay();
	  let dayCondition = 3;//for wednesday
		for (let initialvalue = 1; initialvalue <= limit; initialvalue++) {
            let x = '';
            if (this.isDivisible(initialvalue,3)) {
				if(day === dayCondition)
					x = 'wizz ';
				else
					x = 'fizz ';
			}
            if (this.isDivisible(initialvalue,5)) {
				if(day === dayCondition)
					x = x + 'wuzz';
				else
					x = x + 'buzz';
			}
			if(x !== ''){
                fizzBuzzResult.push(x);
            }
            else{
                fizzBuzzResult.push(initialvalue);
            }
		}
        return fizzBuzzResult;
	}
  render(){
    let listItems = this.FizzBuzz(this.props.value).map(function(element) {
      return (
            <li className={element}>{element}</li>     
      );
    })
  	return (
        <div>
            <ul>
                {listItems}
            </ul>
        </div>
    );
  }
  
  //helper functions, fizzbuzz-specific
  isDivisible(value,divider){
      let result = value % divider;
      return result === 0;
  }; 
}

export default FizzBuzz;