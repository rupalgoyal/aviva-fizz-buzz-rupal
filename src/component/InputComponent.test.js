import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import InputComponent from './InputComponent';
import Adapter from 'enzyme-adapter-react-15';
Enzyme.configure({ adapter: new Adapter() });

let wrapper;

it('InputComponent should contain one input field', () => {
    wrapper = shallow(<InputComponent />);
    expect(wrapper.find('input').length).toEqual(1);
});

it('should have props to accept numbers', () => {
    wrapper = shallow(<InputComponent />);
    expect(wrapper.props().value).defined;
});
